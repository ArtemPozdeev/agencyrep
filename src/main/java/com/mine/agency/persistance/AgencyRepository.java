package com.mine.agency.persistance;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Artem Pozdeev on 23.05.2016.
 */
public class AgencyRepository {
//    public static List<Agency> agencies = new CopyOnWriteArrayList<>();
//    static {
//        agencies.add(new Agency());
//        agencies.add(new Agency(1, "Farmers", "Ukraine"));
//        agencies.add(new Agency(2, "All State", "USA"));
//        agencies.add(new Agency(3, "FCCI Insurance Group", "Italy"));
//        agencies.add(new Agency(4, "Met life", "Ukraine"));
//    }
public static Map<String, String[]> agencies = new ConcurrentHashMap<>();
    static {
        agencies.put("0", new String[]{"Farmers", "Ukraine"});
        agencies.put("1", new String[]{"Farmers", "Ukraine"});
        agencies.put("2", new String[]{"All State", "USA"});
        agencies.put("3", new String[]{"FCCI Insurance Group", "Italy"});
        agencies.put("4", new String[]{"Met life", "Ukraine"});
    }
}
