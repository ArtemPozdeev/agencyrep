package com.mine.agency.model;

/**
 * Created by Artem Pozdeev on 23.05.2016.
 */
public class Agency {
    private int id;
    private String name = "default";
    private String country = "Ukraine";

    public Agency() {
    }
    public Agency(int id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }
    public Agency(String id, String name, String country) {
        this.id = Integer.parseInt(id);
        this.name = name;
        this.country = country;
    }
    public Agency(Agency agency) {
        this.id = agency.id;
        this.name = agency.name;
        this.country = agency.country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
