package com.mine.agency.restControllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Artem Pozdeev on 23.05.2016.
 */
@RestController
public class EmptyController {
    @RequestMapping("/")
    public String getStart(){
        return "Start Rest";
    }
}
