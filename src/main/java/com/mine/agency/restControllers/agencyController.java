package com.mine.agency.restControllers;

import com.mine.agency.model.Agency;
import com.mine.agency.service.AgencyService;
import com.mine.agency.service.AgencyServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Artem Pozdeev on 18.05.2016.
 */
@RestController
@RequestMapping("/agencies")
public class AgencyController {
    AgencyService agencyService = new AgencyServiceImpl();

    @RequestMapping
    public List<Agency> getAgencies()
    {
        return agencyService.getAgencies();
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Agency get(@PathVariable("id") String id) {
        Agency agency = agencyService.get(id);
        return agency;
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        if (agencyService.remove(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> saveAgency(@RequestBody Agency agency) {
        try {
            agencyService.save(agency);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
