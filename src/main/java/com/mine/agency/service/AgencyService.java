package com.mine.agency.service;

import com.mine.agency.model.Agency;

import java.util.List;

/**
 * Created by Artem Pozdeev on 23.05.2016.
 */
public interface AgencyService {
    public List<Agency> getAgencies();
    public Agency get(String id);
    public boolean save(Agency agency);
    public boolean update(Agency agency);
    public boolean remove(String id);
}
