package com.mine.agency.service;

import com.mine.agency.model.Agency;
import com.mine.agency.persistance.AgencyRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artem Pozdeev on 23.05.2016.
 */
public class AgencyServiceImpl implements  AgencyService{
    public List<Agency> getAgencies(){
        List<Agency> agencies = new ArrayList<>();
        for (String id : AgencyRepository.agencies.keySet()){
            agencies.add(new Agency(id, AgencyRepository.agencies.get(id)[0], AgencyRepository.agencies.get(id)[1]));
        }
        return agencies;
    }
    public Agency get(String id) {
        String[] agencyFields = AgencyRepository.agencies.get(id);
        return new Agency(id, agencyFields[0], agencyFields[1]);
    }
    public boolean save(Agency agency) {
        AgencyRepository.agencies.put(String.valueOf(agency.getId()), new String[]{agency.getName(), agency.getCountry()});
        return true;
    }
    public boolean update(Agency agency) {
        AgencyRepository.agencies.put(String.valueOf(agency.getId()), new String[]{agency.getName(), agency.getCountry()});
        return true;
    }
    public boolean remove(String id) {
        if (AgencyRepository.agencies.get(id) != null) {
            AgencyRepository.agencies.remove(id);
            return true;
        } else {
            return false;
        }

    }
}
